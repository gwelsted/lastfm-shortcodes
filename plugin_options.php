<?php
//--------------------------------------------------
//Options Page Code
//--------------------------------------------------

add_action('admin_menu', 'my_plugin_options');


function my_plugin_options() {  
	add_options_page('Last.FM SC', 'Last.FM SC', 'administrator', __FILE__, 'build_options_page');
}

function build_options_page() {  ?>  
	<?php $options = get_option('lfm_plugin_options'); ?>
	<div id="plugin-options-wrap">   
		<div class="icon32" id="icon-tools"> 
			<br /> 
		</div>    
		<h2>Last.FM Shortcodes - Options</h2>    
   
		<form method="post" action="options.php" enctype="multipart/form-data">   
			<?php settings_fields('lfm_plugin_options'); ?>  
			<?php do_settings_sections(__FILE__); ?>
		  <p class="submit">        
			  <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />      
		  </p>    
		</form>  
	</div>
<?php }

add_action('admin_init', 'register_and_build_fields');

function register_and_build_fields() {   
	register_setting('lfm_plugin_options', 'lfm_plugin_options', 'validate_setting');
	add_settings_section('main_section', 'Main Settings', 'section_cb', __FILE__);
	add_settings_field('api_key', 'Last.FM API Key:', 'api_key_setting', __FILE__, 'main_section');
	add_settings_field('cache_time', 'Cache time:', 'cache_setting', __FILE__, 'main_section');
}

//API Key Function
function api_key_setting(){
	$options = get_option('lfm_plugin_options');  
	echo "<input name='lfm_plugin_options[api_key]' type='text' value='{$options['api_key']}' />";
}
//Cache Function
function cache_setting(){
	$options = get_option('lfm_plugin_options');  
	echo "<input name='lfm_plugin_options[cache]' type='text' value='{$options['cache']}' />";

}

function section_cb() {}

function validate_setting($lfm_plugin_options) { 
	return $lfm_plugin_options;
}
?>
