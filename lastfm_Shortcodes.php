<?php
/*
Plugin Name: Last.FM Shortcodes
Plugin URI: http://gwelsted.com
Description: This plugin displays various Artist/Album/Track information in posts or pages using shortcodes, it also caches the LAST.FM info to the local server to reduce bandwidth usage.
Version: 0.2
Author: George Welsted
Author URI: http://gwelsted.com
License: GPL2
*/

/*
Last.FM Shortcodes (Wordpress Plugin)
Copyright (C) 2012 George Welsted
Contact me at http://www.gwelsted.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
define( 'LASTFM_PATH', plugin_dir_path(__FILE__) );
require(LASTFM_PATH . 'plugin_options.php');

$options = get_option('lfm_plugin_options');

$api_key = $options['api_key'];
$handler = "http://ws.audioscrobbler.com/2.0/?method=";
$cache = '0';
$seconds_old = $options['cache_time'];

function load_lastfm_xml($url){

	global $cache;
	
	define( 'LASTFM_PATH', plugin_dir_path(__FILE__) );
		$hash_url = md5($url);
		$filename = LASTFM_PATH . 'cache/'. $hash_url . '.xml';
	
	if (file_exists($filename)){
		$xml = simplexml_load_file($filename);
		$cache = '1';
		
	} else {
		$xml = simplexml_load_file($url);
		$xml -> asXML($filename);
		$cache = '0';
	}
	clear_cache();		
	return $xml;
}	

function clear_cache() {
	global $seconds_old;
	define( 'LASTFM_PATH', plugin_dir_path(__FILE__) );
	$directory = LASTFM_PATH . 'cache';
	
	if( !$dirhandle = @opendir($directory) )
            return;
 
    while( false !== ($filename = readdir($dirhandle)) ) {
            if( $filename != "." && $filename != ".." ) {
                    $filename = $directory. "/". $filename;
 
                    if( @filemtime($filename) < (time()-$seconds_old) )
                            @unlink($filename);
            }
    }
	
}

function list_album_tracks($atts, $content = null){
	global $cache;
	//Pull in Global Settings
	global $api_key;
	global $handler;
	
	//Last.fm API method for this fuction
	$method = "album.getinfo";

	//extract(shortcode_atts(array("artist" => 'null', "album" => 'null'), $atts));
	
	$artist = $atts['artist'];
	$album = $atts ['album'];
	
	$completeurl = $handler.$method."&api_key=".$api_key."&artist=".$artist."&album=".$album;
	
	$xml = load_lastfm_xml($completeurl);
	
	$tracks = $xml->album->tracks->track;
	$return_string = "<ul>";

	for ($i = 0; $i < sizeof($tracks); $i++) {
		$trackname = $tracks[$i]->name;
		$duration = $tracks[$i]->duration / '60';
		$p_duration = round($duration, 2);
		$url = $tracks[$i]->url;
	
	 
		$return_string .= "<li><a href='" . $url . "' target='TOP'>";
	 
		if ($trackname) {
			$return_string .= $trackname. " | " .$p_duration."</br>";
		}
		$return_string .= "</a></li>";
		
		
	}
	$return_string .= "</ul>";
	return $return_string;
}
add_shortcode('albumtracks','list_album_tracks');

//Show Album Art base on Artist, Album, and specified size
function display_album_art($atts){
	//Pull in Global Settings
	global $api_key;
	global $handler;
	
	//Last.fm API method for this fuction
	$method = "album.getinfo";

	//extract(shortcode_atts(array("artist" => 'null', "album" => 'null', "size" => 'large'), $atts));
	
	$artist = $atts['artist'];
	$album = $atts['album'];
	if (isset($atts['size'])){
		$size = $atts['size'];
	} else {
		$size = 'large';
	}
	
	$completeurl = $handler.$method."&api_key=".$api_key."&artist=".$artist."&album=".$album;

	$xml = load_lastfm_xml($completeurl);
	
	$return_string = '<div class="lfm_albumimage_'.$size.'">';
	foreach($xml->album->image as $image){
		$img2 = $xml->album->image->attributes()->size;
		if ($image['size'] == $size) {
			$return_string .= '<img src="'.$image.'">';
		}
	}
	$return_string .= '</div>';
	return $return_string;
}
add_shortcode('albumart','display_album_art');

function display_artist_bio($atts){
	//Pull in Global Settings
	global $api_key;
	global $handler;
	
	//Last.fm API method for this fuction
	$method = "artist.getinfo";

	//extract(shortcode_atts(array("artist" => 'null', "album" => 'null', "size" => 'large'), $atts));
	
	$artist = $atts['artist'];
	if (isset($atts['type'])){
		$type = $atts['type'];
	} else {
		$type = 'summary';
	}
	
	$completeurl = $handler.$method."&api_key=".$api_key."&artist=".$artist;

	$xml = load_lastfm_xml($completeurl);
	
	$summary = $xml->artist->bio->summary;
	$full = $xml->artist->bio->content;
	$biodate = $xml->artist->bio->published;
	
	if ($type == "full"){
		return $full;
	} else if ($type == "summary") {
		return $summary;
	} else if ($type == "date") {
		return $biodate;
	}
}
add_shortcode('artistbio','display_artist_bio');

function display_artist_img($atts){
	//Pull in Global Settings
	global $api_key;
	global $handler;
	
	//Last.fm API method for this fuction
	$method = "artist.getinfo";

	//extract(shortcode_atts(array("artist" => 'null', "album" => 'null', "size" => 'large'), $atts));
	
	$artist = $atts['artist'];
	$class = $atts['class'];

	if (isset($atts['size'])){
		$size = $atts['size'];
	} else {
		$size = 'large';
	}
	
	$completeurl = $handler.$method."&api_key=".$api_key."&artist=".$artist;

	$xml = load_lastfm_xml($completeurl);
	$return_string = '<div class="lfm_artistimage_'.$size.' '.$class.'">';
	foreach($xml->artist->image as $image){
		$img2 = $xml->artist->image->attributes()->size;
		if ($image['size'] == $size) {
			$return_string .= '<img class="'.$class.'" src="'.$image.'">';
		}
	}
	$return_string .= '</div>';
	return $return_string;
}
add_shortcode('artistimg','display_artist_img');

function display_artist_similar($atts){
	//Pull in Global Settings
	global $api_key;
	global $handler;
	
	//Last.fm API method for this fuction
	$method = "artist.getsimilar";

	//extract(shortcode_atts(array("artist" => 'null', "album" => 'null', "size" => 'large'), $atts));
	
	$artist = $atts['artist'];

	if (isset($atts['size'])){
		$size = $atts['size'];
	} else {
		$size = 'large';
	}
	
	$completeurl = $handler.$method."&api_key=".$api_key."&artist=".$artist;

	$xml = load_lastfm_xml($completeurl);
	$return_string = '<div class="lfm_artist_similar">';
	$artists = $xml->similarartists->artist;

	for ($i = 0; $i < 5; $i++) {
		$name = $artists[$i]->name;
		$return_string .= '<p class="sim_artist">'.$name.'</p>';
	}

	$return_string .= '</div>';
	return $return_string;
}
add_shortcode('similarartist','display_artist_similar');

function artist_profile_link($atts){
	$artist = $atts['artist'];
	$class = $atts['class'];
	$return_string = '<a href="/artist-profile/?artist='.$artist.'" class="'.$class.'">Artist Profile</a>';
	return $return_string;
}
add_shortcode('artistprofilelink','artist_profile_link');


//Add Profile Field for Last.FM username (later use)
add_action( 'show_user_profile', 'lastfm_extra_profile_fields' );
add_action( 'edit_user_profile', 'lastfm_extra_profile_fields' );

function lastfm_extra_profile_fields( $user ) { ?>

<h3>Last.FM profile information</h3>

<table class="form-table">
	<tr>
		<th><label for="lastfm">Last.FM</label></th>
		<td>
			<input type="text" name="lastfm" id="lastfm" value="<?php echo esc_attr( get_the_author_meta( 'lastfm', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description">Please enter your Last.FM username.</span>
		</td>
	</tr>
</table>
<?php }

add_action( 'personal_options_update', 'lastfm_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'lastfm_save_extra_profile_fields' );

function lastfm_save_extra_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;
	update_user_meta( $user_id, 'lastfm', $_POST['lastfm'] );
}

?>